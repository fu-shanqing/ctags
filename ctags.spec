Name:		ctags
Version:	5.8
Release:	30
Summary:	A C programming language indexing and/or cross-reference tool

License:	GPLv2+ and LGPLv2+ and Public Domain
URL:		http://ctags.sourceforge.net/
Source0:	http://prdownloads.sourceforge.net/ctags/ctags-5.8.tar.gz

Patch0:		0001-ctags-5.7-destdir.patch
Patch1:		0002-ctags-5.7-segment-fault.patch
Patch2:		0003-ctags-5.8-css.patch
Patch3:		0004-ctags-5.8-ocaml-crash.patch
Patch4:		0005-ctags-5.8-cssparse.patch
Patch5:		0006-ctags-5.8-memmove.patch
Patch6:		0007-ctags-5.8-format-security.patch
Patch7:		0008-ctags-CVE-2014-7204.patch
Patch8:		rename-the-local-__unused__-to-__ctags_unused__.patch
Patch9:		0009-ctags-CVE-2022-4515.patch

BuildRequires:	gcc
Requires:       chkconfig

Provides:       ctags-etags
Obsoletes:      ctags-etags

%description
Ctags generates an index (or tag) file of language objects found in source
files that allows these items to be quickly and easily located by a text
editor or other utility. A tag signifies a language object for which an
index entry is available (or, alternatively, the index entry created for
that object).

This package will also generate tags in a format which GNU Emacs understand,
it's a alternativ implementation of the GNU etags program.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure
%make_build

%install
rm -rf %RPM_BUILD_ROOT
%make_install
cd %{buildroot}%{_bindir}
ln -s ctags etags.ctags
cd %{buildroot}%{_mandir}/man1
ln -s ctags.1 etags.ctags.1

%posttrans
/usr/sbin/alternatives --install /usr/bin/etags emacs.etags /usr/bin/etags.ctags 20 \
   --slave /usr/share/man/man1/etags.1.gz emacs.etags.man /usr/share/man/man1/ctags.1.gz

%postun
/usr/sbin/alternatives --remove emacs.etags /usr/bin/etags.ctags || :

%files
%defattr(-,root,root)
%doc README COPYING
%{!?license: %license %%doc}
%license COPYING
%{_bindir}/ctags
%{_bindir}/etags.ctags

%files help
%doc NEWS FAQ EXTENDING.html
%{_mandir}/man1/ctags.1.gz
%{_mandir}/man1/etags.ctags.1.gz

%changelog
* Thu Nov 28 2024 sqfu <dev01203@linx-info.com> - 5.8-30
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix invalid soft links

* Thu Sep 21 2023 dillon chen <dillon.chen@gmail.com> - 5.8-29
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:CVE-2022-4515

* Mon Aug 16 2021 yixiangzhike <zhangxingliang3@huawei.com> - 5.8-28
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:rename the local __unused__ to __ctags_unused__

* Tue Sep 24 2019 shenyangyang<shenyangyang4@huawei.com> - 5.8-27
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:revise packages and help package

* Thu Sep 12 2019 shenyangyang<shenyangyang4@huawei.com> - 5.8-26
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix the alternative bug after uninstall ctags(bug 17)

* Mon Aug 12 2019 openEuler Builteam <buildteam@openeuler.org> - 5.8-25
- Package init
